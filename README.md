
A very bare-bones REST chatboard app

Docker-compose is used to run the app and to download and run a mongoDb image easily

The mongoDb database stores the users and the messages they send to the chatboard
Users have to login and get their "Bearer" tokens from the /auth/token endpoint

Typical flow:
    User1 is registered - POST /users
    User1 logs in - POST /auth/token
    User1 checks the messages GET /messages 
    User1 adds a message POST /messages
    User2 is registered - POST /users
    User2  logs in - POST /auth/token
    User2 checks the messages GET /messages 
    User2 responds to User1 POST /messages 

How to run: 
docker-compose up

That's it - API should be accessible on localhost:8080

Things that I would improve - 
    add some nice error handling
    add request model validations
    use web sockets to stream live chat    