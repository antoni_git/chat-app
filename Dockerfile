FROM node:10

WORKDIR /src

COPY package*.json ./

RUN npm install -no-cache

COPY . .

EXPOSE 8080

CMD [ "npm", "run", "start" ]
