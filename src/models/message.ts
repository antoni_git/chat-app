import * as mongoose from "mongoose";

const MessageSchema = new mongoose.Schema({
    text: {type: String, required: true},
    sender: {type: String, required: true}
})

export interface IMessage extends mongoose.Document {
    text: string,
    sender: string
}

export const MessageModel = mongoose.model("Messages", MessageSchema)