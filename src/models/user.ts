import * as mongoose from "mongoose";

const UserSchema = new mongoose.Schema({
    username: {type: String, unique: true, required: true},
    password: {type: String, required: true}
})

export interface IUser extends mongoose.Document {
    username: string,
    password: string
}

export const UserModel = mongoose.model("Users", UserSchema)