import { IUser, UserModel } from "../models/user"
import * as crypto from "crypto" 
import { rejects } from "assert";

export class UserService {

    public async addUser(user: IUser): Promise<void>{
        return new Promise((resolve, reject) => {
            let salt = crypto.randomBytes(16).toString('base64');
            let hash = crypto.createHmac('sha512',salt)
                .update(user.password)
                .digest("base64");
            user.password = salt + "$" + hash;
            const newUser = new UserModel({...user})
            newUser.save().then(() => {
                resolve()
            }).catch((err) => {
                reject(err)
                console.log(err)
            })
        })
    } 

    public async findByUsername(username: string): Promise<IUser> {
        return new Promise<IUser>((resolve, reject) => {
            UserModel.findOne({username: username}).then((result) => {
                resolve(result as IUser)
            }).catch((err) => {
                reject(err)
            })
        })
    }
}