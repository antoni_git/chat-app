import { IMessage, MessageModel } from "../models/message"

export class MessageService {

    public async addMessage(message: IMessage, sender: string): Promise<void>{
        return new Promise((resolve, reject) => {            
            const newMessage = new MessageModel({...message, sender: sender})
            newMessage.save().then(() => {
                resolve()
            }).catch((err) => {
                reject(err)
                console.log(err)
            })
        })
    } 

    public async getAllMessages(): Promise<any>{
        return new Promise((resolve, reject) => {                        
            MessageModel.find().then((result) => { 
                resolve((result as IMessage[]).map(message => Object.assign( {}, { text: message.text, sender: message.sender })))
            }).catch((err) => {
                reject(err)
                console.log(err)
            })
        })
    } 
}