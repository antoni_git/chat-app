import * as crypto from "crypto"
import * as jwt from "jsonwebtoken" 

export const generateToken = (body: any): Promise<any> => {
    return new Promise((resolve, reject) => {
        try{
            console.log("IN JWT");
            let refreshId = body.userId + process.env.JWT_SECRET;
            let salt = crypto.randomBytes(16).toString("base64");
            let hash = crypto.createHmac("sha512", salt).update(refreshId).digest("base64");
            body.refreshKey = salt; 
            let token = jwt.sign(body,
                process.env.JWT_SECRET);
            const decode = jwt.decode(token)
            console.log(decode)
            let b = new Buffer(hash);
            let refreshToken = b.toString("base64");
            resolve({accessToken: token, refreshToken: refreshToken})
        } catch (err){
            reject(err)
        }
    })       
} 