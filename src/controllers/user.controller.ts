import { UserService } from "../services/user.service";
import { Request, Response, Router, NextFunction } from "express";

export class UserController{
    private router: Router

    constructor(private userService: UserService){
        this.initRouter()
    }

    initRouter(){
        this.router = Router()
        this.router.post("/", (req: Request, res: Response, next: NextFunction) => { this.registerUser(req,res,next) }) 
    }
    /*
        Register a new user
        Request body:
        {
            "username" : "username"
            "password" : "password"
        }
    */
    private async registerUser(req: Request, res: Response, next: NextFunction) {
        await this.userService.addUser(req.body).then((result) => {
            res.status(200).send({ message: "User registered successfully" })
        }).catch((err) => {
            res.status(500).send({errors: err})
        })
    } 

    getRouter(){
        return this.router
    }
}