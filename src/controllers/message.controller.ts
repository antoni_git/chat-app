import { Request, Response, Router, NextFunction } from "express";
import { MessageService } from "../services/message.service";
import { resolveUsername } from "../middleware/resolve.username";
import { validJWTNeeded } from "../middleware/valid.token.needed";

export class MessageController{
    private router: Router

    constructor(private messageService: MessageService){
        this.initRouter()
    }

    initRouter(){
        this.router = Router()
        this.router.post("/", validJWTNeeded ,resolveUsername,
         (req: Request, res: Response, next: NextFunction) => { this.addMessage(req,res,next) }) 
        this.router.get("/", validJWTNeeded,
         (req: Request, res: Response, next: NextFunction) => { this.getMessages(req,res,next) }) 
    }
    /*
        Send a message to the chatboard
        Header:
            Key: Authorization
            Value: Bearer {access token - generated from /auth/token endpoint}
        Request body:
        {
            "text" : "message contents"
        }
    */
    private async addMessage(req: Request, res: Response, next: NextFunction) {
        await this.messageService.addMessage(req.body, req.body.username).then((result) => {
            res.status(200).send({ message: "Message added"})
        }).catch((err) => {
            res.status(500).send({errors: err})
        })
    } 

    /*
        List all messages
        Header:
            Key: Authorization
            Value: Bearer {access token - generated from /auth/token endpoint}
    */
    private async getMessages(req: Request, res: Response, next: NextFunction) {
        await this.messageService.getAllMessages().then((result) => {
            res.status(200).send(result)
        }).catch((err) => {
            res.status(500).send({errors: err})
        })
    } 

    getRouter(){
        return this.router
    }
}