import { Request, Response, Router, NextFunction } from "express";
import { verifyUser } from "../middleware/verify.user";
import { generateToken } from "../utils/generate.token";
import { UserService } from "../services/user.service";

export class AuthController{
    private router: Router

    constructor(private userService: UserService){
        this.initRouter()
    }

    initRouter(){
        this.router = Router()
        this.router.post("/token", [verifyUser(this.userService),
            (req: any, res: any, next: any) => { this.getToken(req, res, next) }]);
    }

    /*
        Generate an access token for the user. Token must be provided in order to use the message endpoints.
        Request body:
        {
            "username" : "username"
            "password" : "password"
        }
    */
    private async getToken(req: Request, res: Response, next: NextFunction){
        await generateToken(req.body).then((result) => {
            res.status(201).send(result)
        }).catch((err) => {
            console.log(err)
            res.status(500).send({errors: err});
        })
    }    

    public getRouter(){
        return this.router;
    }
}