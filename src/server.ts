import * as express from "express"
import * as mongoose from "mongoose" 
import bodyParser = require("body-parser") 
import { UserController } from "./controllers/user.controller"
import { AuthController } from "./controllers/auth.controller"
import { UserService } from "./services/user.service" 
import { MessageController } from "./controllers/message.controller"
import { MessageService } from "./services/message.service"

const dotenv = require('dotenv');
dotenv.config();

class Server {
    private app: express.Application

    constructor(private userController: UserController,
      private authController: AuthController,
      private messageController: MessageController) { 
        this.app = express()
        this.app.use(bodyParser.json())
        
        this.app.use("/users", this.userController.getRouter())
        this.app.use("/auth", this.authController.getRouter())
        this.app.use("/messages", this.messageController.getRouter())

        this.connect()
        this.startServer()
    }

    connect() {
        mongoose
      .connect(
        "mongodb://storage_db:27017/test",
        { useNewUrlParser: true }
      )
      .then(() => {
        return console.info(`Successfully connected to 
        "mongodb://storage_db:27017/test"`);
      })
      .catch(error => {
        console.error("Error connecting to database: ", error);
        return process.exit(1);
      });
    }
    
    startServer() {
        this.app.listen(8080, () => {
            console.log("Listening on port 8080")
        })
    }
}

const server = new Server(new UserController(new UserService()),
   new AuthController(new UserService()),
   new MessageController(new MessageService()))