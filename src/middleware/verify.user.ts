import * as crypto from "crypto"
import { UserService } from "../services/user.service";
import bodyParser = require("body-parser");

export function verifyUser(userService: UserService){
    return (req: any, res: any, next: any) => {
        userService.findByUsername(req.body.username)
            .then((user) => {
                    let passwordFields = user.password.split("$");
                    let salt = passwordFields[0];
                    let hash = crypto.createHmac("sha512", salt).update(req.body.password).digest("base64");
                    if (hash === passwordFields[1]) {
                        req.body.userId = user._id
                        return next();
                    } else {
                        return res.status(400).send({errors: ["Invalid username or password"]});
                    }
            }).catch((error) => {
                return res.status(400).send({errors: [error]}); 
        })};
}