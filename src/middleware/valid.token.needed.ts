import * as jwt from "jsonwebtoken" 

export const validJWTNeeded = (req: any, res: any, next: any) => {
    if (req.headers["authorization"]) {
        try {
            let authorization = req.headers["authorization"].split(' ');
            if (authorization[0] !== "Bearer") {
                return res.status(401).send();
            } else {
                req.jwt = jwt.verify(authorization[1], process.env.JWT_SECRET);
                return next();
            }
        } catch (err) { 
            return res.status(403).send("Malformed or invalid token");
        }
    } else {
        return res.status(401).send("Unauthorized token");
    }
};