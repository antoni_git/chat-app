import * as jwt from "jsonwebtoken" 
//Get the username from the token for the message.sender field
export const resolveUsername = (req: any, res: any, next: any) => {
    const decode = jwt.decode(req.headers["authorization"].split(' ')[1])
    req.body.username = (decode as any).username
    next()
};